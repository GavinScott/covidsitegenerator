export interface DataPoint {
  t: string
  c: number
  d: number
  nc: number
  nd: number
}

export type CountyData = Record<string, Record<string, DataPoint[]>>

export type DataJson = { date: string; data: CountyData }
