import { DataJson } from './data/data'
const lzw = require('lzwcompress')

const dataUrl = 'https://covid-19-new-york-times-data-cache.s3.us-east-1.amazonaws.com/data.json'

export interface S3Data {
  error: boolean
  data?: DataJson
}

export const getCovidData = async (): Promise<S3Data> => {
  try {
    const res = await fetch(dataUrl)
    const response = await res.text()
    return { error: false, data: JSON.parse(lzw.unpack(JSON.parse(response))) }
  } catch (err) {
    console.log('Unable to load S3 data')
    console.log(err)
    return {
      error: true,
    }
  }
}
