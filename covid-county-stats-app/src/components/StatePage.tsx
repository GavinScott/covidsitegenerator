import React from 'react'
import { startCase } from 'lodash'
import { CountyData } from '../data/data'
import StatsGraphs from './StatsGraphs'

interface StatePageProps {
  params: Record<string, string>
  countyData: CountyData
}

const stateAbbreviations: Record<string, string> = {
  AL: 'Alabama',
  AK: 'Alaska',
  AS: 'American Samoa',
  AZ: 'Arizona',
  AR: 'Arkansas',
  CA: 'California',
  CO: 'Colorado',
  CT: 'Connecticut',
  DE: 'Delaware',
  DC: 'District Of Columbia',
  FM: 'Federated States Of Micronesia',
  FL: 'Florida',
  GA: 'Georgia',
  GU: 'Guam',
  HI: 'Hawaii',
  ID: 'Idaho',
  IL: 'Illinois',
  IN: 'Indiana',
  IA: 'Iowa',
  KS: 'Kansas',
  KY: 'Kentucky',
  LA: 'Louisiana',
  ME: 'Maine',
  MH: 'Marshall Islands',
  MD: 'Maryland',
  MA: 'Massachusetts',
  MI: 'Michigan',
  MN: 'Minnesota',
  MS: 'Mississippi',
  MO: 'Missouri',
  MT: 'Montana',
  NE: 'Nebraska',
  NV: 'Nevada',
  NH: 'New Hampshire',
  NJ: 'New Jersey',
  NM: 'New Mexico',
  NY: 'New York',
  NC: 'North Carolina',
  ND: 'North Dakota',
  MP: 'Northern Mariana Islands',
  OH: 'Ohio',
  OK: 'Oklahoma',
  OR: 'Oregon',
  PW: 'Palau',
  PA: 'Pennsylvania',
  PR: 'Puerto Rico',
  RI: 'Rhode Island',
  SC: 'South Carolina',
  SD: 'South Dakota',
  TN: 'Tennessee',
  TX: 'Texas',
  UT: 'Utah',
  VT: 'Vermont',
  VI: 'Virgin Islands',
  VA: 'Virginia',
  WA: 'Washington',
  WV: 'West Virginia',
  WI: 'Wisconsin',
  WY: 'Wyoming',
}

export const getState = (input: string): string | undefined => {
  if (input.length === 2) {
    return stateAbbreviations[input.toUpperCase()].toLowerCase()
  }
  const state = startCase(input)
  return Object.values(stateAbbreviations).includes(state) ? state.toLowerCase() : undefined
}

export default class StatePage extends React.PureComponent<StatePageProps, {}> {
  render() {
    const state = getState(this.props.params.state)
    return !state ? (
      <p>"{startCase(this.props.params.state)}" is not a valid U.S. state!</p>
    ) : (
      <div>
        <h1>{startCase(state)} Statistics</h1>
        <hr />
        <StatsGraphs countyData={this.props.countyData} state={state} />
      </div>
    )
  }
}
