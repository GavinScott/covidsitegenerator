import React from 'react'
import { Line } from 'react-chartjs-2'

interface GraphProps {
  title: string
  labels: string[]
  datasets: Dataset[]
}

interface Dataset {
  label: string
  data: any[]
}

const colors = ['75,192,192', '153,153,255']

const generateDataSet = (dataset: Dataset, color: string) => {
  return {
    label: dataset.label,
    fill: false,
    lineTension: 0.1,
    backgroundColor: `rgba(${color},0.4)`,
    borderColor: `rgba(${color},1)`,
    borderCapStyle: 'butt',
    borderDash: [],
    borderDashOffset: 0.0,
    borderJoinStyle: 'miter',
    pointBorderColor: `rgba(${color},1)`,
    pointBackgroundColor: '#fff',
    pointBorderWidth: 1,
    pointHoverRadius: 5,
    pointHoverBackgroundColor: `rgba(${color},1)`,
    pointHoverBorderColor: `rgba(220,220,220,1)`,
    pointHoverBorderWidth: 2,
    pointRadius: 1,
    pointHitRadius: 10,
    data: dataset.data,
  }
}

export default class Graphs extends React.PureComponent<GraphProps, {}> {
  render() {
    const data = {
      labels: this.props.labels,
      datasets: this.props.datasets.map((_, ndx: number) => generateDataSet(_, colors[ndx])),
    }
    return (
      <div>
        <h2>{this.props.title}</h2>
        <div className="graph">
          <Line data={data} options={{ maintainAspectRatio: false }} />
        </div>
      </div>
    )
  }
}
