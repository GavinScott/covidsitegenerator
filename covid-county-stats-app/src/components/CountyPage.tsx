import React from 'react'
import { getState } from './StatePage'
import { startCase } from 'lodash'
import StatsGraphs from './StatsGraphs'
import { CountyData } from '../data/data'

interface CountyPageProps {
  params: Record<string, string>
  countyData: CountyData
}

export const getCounty = (
  input: string,
  state: string,
  countyData: CountyData
): string | undefined => {
  const county = input.toLowerCase()
  return Object.keys(countyData[state]).includes(county) ? county : undefined
}

export default class CountyPage extends React.PureComponent<CountyPageProps, {}> {
  render() {
    const state = getState(this.props.params.state)
    if (!state) {
      return <p>"{startCase(this.props.params.state)}" is not a valid U.S. state!</p>
    }
    const county = getCounty(this.props.params.county, state, this.props.countyData)
    if (!county) {
      return (
        <p>
          "{startCase(this.props.params.county)}" is not a valid county in {state}
        </p>
      )
    }
    return (
      <div>
        <h1>{`${startCase(county)} County Statistics`}</h1>
        <hr />
        <StatsGraphs countyData={this.props.countyData} state={state} county={county} />
      </div>
    )
  }
}
