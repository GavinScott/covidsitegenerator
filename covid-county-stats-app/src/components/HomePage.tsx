import React from 'react'
import { CountyData } from '../data/data'
import StatsGraphs from './StatsGraphs'

interface HomePageProps {
  countyData: CountyData
}

export default class HomePage extends React.PureComponent<HomePageProps, {}> {
  render() {
    return (
      <div>
        <h1>United States National Statistics</h1>
        <hr />
        <StatsGraphs countyData={this.props.countyData} />
      </div>
    )
  }
}
