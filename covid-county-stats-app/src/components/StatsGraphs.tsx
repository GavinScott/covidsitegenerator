import React from 'react'
import Graph from './Graph'
import { CountyData, DataPoint } from '../data/data'

interface StatsGraphsProps {
  countyData: CountyData
  state?: string
  county?: string
}

let nationalData: DataPoint[]
let stateData: Record<string, DataPoint[]> = {}

const getNationalData = (countyData: CountyData): DataPoint[] => {
  if (!nationalData) {
    const data: Record<string, DataPoint> = {}
    Object.values(countyData).forEach((state) => {
      Object.values(state).forEach((county) => {
        county.forEach((_) => {
          if (!data[_.t]) {
            data[_.t] = {
              t: _.t,
              c: 0,
              d: 0,
              nc: 0,
              nd: 0,
            }
          }
          data[_.t].c += _.c
          data[_.t].d += _.d
          data[_.t].nc += _.nc
          data[_.t].nd += _.nd
        })
      })
    })
    nationalData = Object.values(data)
  }
  return nationalData
}

const getStateData = (countyData: CountyData, state: string): DataPoint[] => {
  if (!stateData[state]) {
    const raw = countyData[state]
    const data: Record<string, any> = {}
    Object.values(raw).forEach((county) => {
      county.forEach((_) => {
        if (!data[_.t]) {
          data[_.t] = {
            t: _.t,
            c: 0,
            d: 0,
            nc: 0,
            nd: 0,
          }
        }
        data[_.t].c += _.c
        data[_.t].d += _.d
        data[_.t].nc += _.nc
        data[_.t].nd += _.nd
      })
    })
    stateData[state] = Object.values(data)
  }
  return stateData[state]
}

const getData = (
  countyData: CountyData,
  config: { state?: string; county?: string }
): DataPoint[] => {
  if (config.state && config.county) {
    return countyData[config.state][config.county]
  }
  if (config.state) {
    return getStateData(countyData, config.state)
  }
  return getNationalData(countyData)
}

export default class StatsGraphs extends React.PureComponent<StatsGraphsProps, {}> {
  render() {
    const data = getData(this.props.countyData, {
      state: this.props.state,
      county: this.props.county,
    })
    const labels = data.map((_: any) => _.t)
    return (
      <div>
        <Graph
          title="Total Cases & Deaths"
          labels={labels}
          datasets={[
            {
              label: 'Total Cases',
              data: data.map((_: any) => _.c),
            },
            {
              label: 'Total Deaths',
              data: data.map((_: any) => _.d),
            },
          ]}
        />
        <Graph
          title="New Cases & Deaths"
          labels={labels}
          datasets={[
            {
              label: 'New Cases',
              data: data.map((_: any) => _.nc),
            },
            {
              label: 'New Deaths',
              data: data.map((_: any) => _.nd),
            },
          ]}
        />
      </div>
    )
  }
}
