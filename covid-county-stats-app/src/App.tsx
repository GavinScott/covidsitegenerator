import React from 'react'
import './App.css'
import CountyPage from './components/CountyPage'
import StatePage from './components/StatePage'
import { HashRouter as Router, Switch, Route, Link } from 'react-router-dom'
import HomePage from './components/HomePage'
import { DataJson } from './data/data'
import { getCovidData, S3Data } from './getData'
import { push as Menu } from 'react-burger-menu'
import { startCase } from 'lodash'

interface LinkDetail {
  label: string
  href: string
}

interface AppState {
  s3Data?: S3Data
}

export default class App extends React.PureComponent<{}, AppState> {
  _asyncRequest: any = null

  constructor(props: {}) {
    super(props)
    this.state = {}
  }

  componentWillMount() {
    this._asyncRequest = getCovidData().then((s3Data: any) => {
      this._asyncRequest = null
      this.setState({ s3Data })
    })
  }

  componentWillUnmount() {
    if (this._asyncRequest) {
      this._asyncRequest.cancel()
    }
  }

  getBody() {
    if (!this.state.s3Data || !this.state.s3Data.data) {
      return <h1>Loading...</h1>
    }
    if (this.state.s3Data.error) {
      return <h1>Something went wrong :(</h1>
    }
    const dataJson: DataJson = this.state.s3Data.data
    return (
      <Switch>
        <Route path="/:state/:county">
          {({ match }) => {
            return <CountyPage countyData={dataJson.data} params={match!.params} />
          }}
        </Route>
        <Route path="/:state">
          {({ match }) => {
            return <StatePage countyData={dataJson.data} params={match!.params} />
          }}
        </Route>
        <Route path="/">
          <HomePage countyData={dataJson.data} />
        </Route>
      </Switch>
    )
  }

  getRefreshMessage() {
    if (!this.state.s3Data || !this.state.s3Data.data) {
      return ''
    }
    const dataJson: DataJson = this.state.s3Data.data
    const refreshDate = new Date(dataJson.date)
    const refreshDelta = Math.round((new Date().getTime() - refreshDate.getTime()) / 60000)
    let deltaString
    if (refreshDelta < 60) {
      deltaString = `${refreshDelta} minute${refreshDelta !== 1 ? 's' : ''}`
    } else {
      const hoursDelta = Math.floor(refreshDelta / 60)
      const hoursString = `${hoursDelta} hour${hoursDelta === 1 ? '' : 's'}`
      const minutesDelta = refreshDelta % 60
      deltaString = minutesDelta
        ? `${hoursString}, ${minutesDelta} minute${minutesDelta === 1 ? '' : 's'}`
        : hoursString
    }
    return `Data refreshed at ${refreshDate.toLocaleString()} (${deltaString} ago)`
  }

  render() {
    const navPages =
      !this.state.s3Data || !this.state.s3Data.data ? (
        <div />
      ) : (
        <div>
          {Object.keys(this.state.s3Data.data.data)
            .sort()
            .map((state) => {
              return (
                <details>
                  <summary>{startCase(state)}</summary>
                  <ul>
                    <li>
                      <Link to={`/${state}`}>{startCase(state)} State</Link>
                    </li>
                    {Object.keys(this.state.s3Data!.data!.data[state])
                      .sort()
                      .map((_) => {
                        return (
                          <li>
                            <Link to={`/${state}/${_}`}>{startCase(_)} County</Link>
                          </li>
                        )
                      })}
                  </ul>
                </details>
              )
            })}
        </div>
      )
    return (
      <div id="outer-container">
        <Router>
          <Menu isOpen={false} pageWrapId={'page-wrap'} outerContainerId={'outer-container'}>
            <h1 className="navheader">
              <b>
                <Link to="/">Covid-19 Stats</Link>
              </b>
            </h1>
            <Link to="/">Back to U.S. national data</Link>
            <hr />
            {navPages}
          </Menu>
          <div id="page-wrap" className="main">
            {this.getBody()}
            <div className="footer">
              <hr />
              <p>
                This site uses data from The New York Times, based on reports from state and local
                health agencies, which is stored in a{' '}
                <a href="https://github.com/nytimes/covid-19-data">GitHub repository</a>. This site
                refreshes its data from their repository multiple times a day. You can see their
                U.S. tracking page{' '}
                <a href="https://www.nytimes.com/interactive/2020/us/coronavirus-us-cases.html">
                  here.
                </a>
              </p>
              <p>{this.getRefreshMessage()}</p>
            </div>
          </div>
        </Router>
      </div>
    )
  }
}
