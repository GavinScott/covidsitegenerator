import { existsSync } from 'fs'
import AWS from 'aws-sdk'
import { readFileSync } from 'fs'
const lzw = require('lzwcompress')
const { exec } = require('child_process')

const s3 = new AWS.S3({ region: 'us-east-1' })

// arn:aws:lambda:us-east-1:553035198032:layer:git:5

const execShellCommand = async (cmd: string) => {
  console.log(`Running command: ${cmd}`)
  console.log(
    await new Promise((resolve, _reject) => {
      exec(cmd, (error: any, stdout: any, stderr: any) => {
        if (error) {
          console.log('There was an error execing this command')
          throw error
        }
        resolve(stdout ? stdout : stderr)
      })
    })
  )
}

const syncRepo = async (url: string) => {
  console.log(`\nSyncing repository: ${url}`)
  const repoNameMatch = url.match('.*/([a-zA-Z0-9-]+).git')
  if (!repoNameMatch) {
    throw Error(`could not get repository name from url: ${url}`)
  }
  const repoDir = `/tmp/${repoNameMatch[1]}`
  if (!existsSync(repoDir)) {
    const git = require('simple-git/promise')('/tmp')
    console.log('Cloning repository')
    console.log(await git.clone(url))
  } else {
    const git = require('simple-git/promise')(repoDir)
    console.log('Pulling updates to repository')
    console.log(await git.pull())
  }
}

const formatDataForS3 = () => {
  const rows = readFileSync('/tmp/covid-19-data/us-counties.csv', { encoding: 'utf8' })
    .split('\n')
    .slice(1)

  const data: Record<
    string,
    Record<string, Array<{ t: string; c: number; d: number; nc: number; nd: number }>>
  > = {}

  rows.forEach((_) => {
    let [t, county, state, fips, cases, deaths] = _.split(',')
    county = county.toLowerCase()
    state = state.toLowerCase()
    fips
    if (!(state in data)) {
      data[state] = {}
    }
    if (!(county in data[state])) {
      data[state][county] = []
    }
    const totalCases = parseInt(cases, 10)
    const totalDeaths = parseInt(deaths, 10)
    const previous = data[state][county].length
      ? data[state][county][data[state][county].length - 1]
      : { c: 0, d: 0 }
    data[state][county].push({
      t,
      c: totalCases,
      d: totalDeaths,
      nc: totalCases - previous.c,
      nd: totalDeaths - previous.d,
    })
  })

  return JSON.stringify(lzw.pack(JSON.stringify({ date: new Date().toString(), data })))
}

exports.handler = async (_event: any) => {
  await syncRepo('https://github.com/nytimes/covid-19-data.git')
  await execShellCommand('ls /tmp/covid-19-data')
  console.log('Putting in S3')
  await s3
    .putObject({
      Body: formatDataForS3(),
      Bucket: 'covid-19-new-york-times-data-cache',
      Key: 'data.json',
      ACL: 'public-read',
    })
    .promise()
  console.log('Done, data updated successfully')
}

// for running locally
if (require.main === module) {
  exports.handler({}).then(console.log).catch(console.log)
}
