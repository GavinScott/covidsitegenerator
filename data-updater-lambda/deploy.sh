rm -rf dist
rm -rf lambdaFunction.zip
npm install
npm run build
cp -r node_modules/ dist/
cd dist
zip -r ../lambdaFunction.zip .
cd ..
aws lambda update-function-code --region us-east-1 --function-name CovidSiteDataUpdateLambda --zip-file fileb://./lambdaFunction.zip
echo 'Done'
